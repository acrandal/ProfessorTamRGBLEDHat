///////////////////////////////////////////////////////////////////////////////////////////
//
// ALA library example: RgbStripButton
//
// Example to demonstrate how to switch animations using three buttons.
// The first button changes the animation.
// The second button changes the color palette.
// The third button changes the animation speed.
//
// Button library is required: http://playground.arduino.cc/Code/Button
//
// Web page: http://yaab-arduino.blogspot.com/p/ala-example-rgbstripbutton.html
//
// Minor modifications for my situation and hat dev:
//  Aaron S. Crandall <acrandal@gmail.com> - 2018
//
///////////////////////////////////////////////////////////////////////////////////////////
#include "AlaLedRgb.h"
#include "Button.h"


AlaLedRgb rgbStrip;
Button button1 = Button(2, PULLUP);
Button button2 = Button(3, PULLUP);
Button button3 = Button(4, PULLUP);

int animation = 2;
int duration = 2;
int palette = 3;


int LED_BRIGHTNESS_MASK = 0x0A0A0A;

int animList[14] = {
    ALA_ON,
    ALA_SPARKLE,
    ALA_SPARKLE2,
    ALA_PIXELSHIFTRIGHT,
    ALA_PIXELSMOOTHSHIFTRIGHT,
    ALA_MOVINGBARS,
    ALA_COMET,
    ALA_COMETCOL,
    ALA_GLOW,
    ALA_CYCLECOLORS,
    ALA_FADECOLORS,
    ALA_FIRE,
    ALA_BOUNCINGBALLS,
    ALA_BUBBLES
};

AlaColor alaPalWSU_[] =
{
//    0x981E32,
//  0xGRB
    0x22EE11,
    0x332222
};
AlaPalette alaPalWSU = { 2, alaPalWSU_ };


AlaPalette paletteList[4] = {
    alaPalRgb,
    alaPalRainbow,
    alaPalFire,
    alaPalWSU
};

int durationList[3] = {
    1000,
    2000,
    5000
};


void setup()
{
  delay(500);
  
  rgbStrip.initWS2812(30, 6);
  rgbStrip.setBrightness(0x0A0A0A);

  updateAnimation();
}

void loop()
{
  // button 1 changes the animation
  if (button1.uniquePress())
  {
    animation++;
    updateAnimation();
  }
  
  // button 2 changes the color palette
  if (button2.uniquePress())
  {
    palette++;
    updateAnimation();
  }
  
  // button 3 changes the animation speed
  if (button3.uniquePress())
  {
    duration++;
    updateAnimation();
  }
  
  rgbStrip.runAnimation();

}

void updateAnimation()
{
    rgbStrip.setAnimation(animList[animation%14], durationList[duration%3], paletteList[palette%4]);
}
