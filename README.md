# Professor's Commencement Tam (hat)

A demo hat for commencement. In this case, it's a tam for us professor type people. The key components are a battery in the pocket, an arduino on top, and a strip of LEDs sewn onto the brim to make some cool light displays to show off while shaking hands and congratulating our new grads!

Parts:
* Arduino Uno
* 1m 30 RGB LED light strip
* Arduino proto shield
* 3 buttons
* some wire
* thread 
* a few safety pins
* USB A to B cable
* USB battery pack
* ALA Arduino library
* An old (unsupported) Arduino Button library (used by the ALA examples)

Created: 2018.05.04
Used: Washington State University Spring 2018 Commencement
By: Aaron S. Crandall, PhD  <acrandal@gmail.com>


I did try this on an Adafruit Feather Proto 32u4, but it could only drive about 8 or 9 of the lights before the voltage wasn't enough from the Batt pin. With the Uno it drove everything quite nicely, though I've got a loose wire somewhere that makes it drop off or reboot when I shake it or lean it just right. 
